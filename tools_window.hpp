#pragma once

#ifndef dk_tools_window_hpp
#define dk_tools_window_hpp

#include <experimental/filesystem>
#include "window.hpp"
#include "game_view_window.hpp"
#include "play_window.hpp"
#include <map>
#include <string>

//#include <string>
//#include "imgui/imgui.h"

namespace fs = std::experimental::filesystem;

namespace dk {
  class ToolsWindow : public Window {
  public:

    const char* TOOLS_WINDOW_NAME = "Tools";
    const char* TOOLS_WINDOW_DIRECTORY = "tools";
    const char* GROUNDS_DIRECTORY = "tools\\grounds";
    const char* UNITS_DIRECTORY = "tools\\players";

    void init() {
      load_tools();
      init_texture();
      init_default_texture();
      init_default_player_texture();
    }

    void init_default_texture() {
      fs::path current(std::experimental::filesystem::current_path());
      fs::path grounds_path(std::experimental::filesystem::current_path() / fs::path(GROUNDS_DIRECTORY));
      std::string key = category_images.at(grounds_path.u8string())->at(0);
      cw::Texture* texture = image_texture.at(key);
      if (texture != nullptr)
        GameViewWindow::getInstance()->setDefaultTexture(texture->tID);
    }

    static ToolsWindow* getInstance() {
      if (instance == nullptr) { instance = new ToolsWindow(); }
      return instance;
    }

    void draw() {
      ImGui::Begin(TOOLS_WINDOW_NAME);

      for (auto it = category_images.begin(); it != category_images.end(); it++) {
        if (ImGui::CollapsingHeader(it->first.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
          for (auto itt = it->second->begin(); itt != it->second->end(); itt++) {
            cw::Texture* texture = image_texture.at(*itt);
            ImGui::Text(
              fs::path(*itt).filename().u8string().c_str()
            );
            texture->bind();
            if (ImGui::ImageButton((void*)(texture->tID), ImVec2(32, 32))) {
              std::cout << "click" << std::endl;
              dk::GameViewWindow::getInstance()->setTexture(texture);
            }
            texture->unbind();
          }
        }
      }

      ImGui::End();
    }



  private:
    static ToolsWindow* instance;
    ToolsWindow() { init(); }

    void load_tools() {
      fs::path current(std::experimental::filesystem::current_path());
      fs::path tools_path(std::experimental::filesystem::current_path() / fs::path(TOOLS_WINDOW_DIRECTORY));

      for (auto& entry : fs::directory_iterator(tools_path)) {

        std::vector<std::string>* vector = new std::vector<std::string>();

        category_images.insert(
          std::make_pair(entry.path().u8string(), vector)
        );

        for (auto& entry : fs::directory_iterator(entry.path())) {
          vector->push_back(entry.path().u8string());
        }
      }
    }

    void init_texture() {
      for (auto it = category_images.begin(); it != category_images.end(); it++) {
        for (auto itt = it->second->begin(); itt != it->second->end(); itt++) {
          cw::Image* img = new cw::Image(itt->c_str());
          cw::Texture* texture = new cw::Texture(img->width, img->height, img->channel);
          texture->bind();
          texture->update(img->data);
          texture->unbind();

          image_texture.insert(
            std::make_pair(*itt, texture)
          );
          delete(img);
        }
      }
    }



    void init_default_player_texture() {
      fs::path current(std::experimental::filesystem::current_path());
      fs::path grounds_path(std::experimental::filesystem::current_path() / fs::path(UNITS_DIRECTORY));
      std::string key = category_images.at(grounds_path.u8string())->at(0);
      cw::Texture* texture = image_texture.at(key);
      if (texture != nullptr)
        GameViewWindow::getInstance()->setPlayerTexture(texture);
    }

    virtual ~ToolsWindow() {
      for (auto it = category_images.begin(); it != category_images.end(); it++) {
        delete(it->second);
      }

      for (auto it = image_texture.begin(); it != image_texture.end(); it++) {
        delete(it->second);
      }
    }

    std::map<std::string, std::vector<std::string>*> category_images;
    std::map<std::string, cw::Texture*> image_texture;

  };

  ToolsWindow* ToolsWindow::instance = nullptr;
}
#endif
