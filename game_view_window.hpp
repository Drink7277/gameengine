#pragma once

#ifndef dk_game_view_window_hpp
#define dk_game_view_window_hpp

#include "window.hpp"
#include <string>

namespace dk {

  struct GridData{
    bool is_select = false;
    unsigned int tid = NULL;
    unsigned int building_tid = NULL;
    unsigned int unit_tid = NULL;
  };

  class GameViewWindow : public Window {
  public:

    const char* GAME_VIEW_WINDOW_NAME = "Game View";
    const float TEXT_INPUT_WIDTH = 30.0f;
    const ImVec4 DEACTIVE_COLOR = ImVec4(1, 0, 0, 1);
    const ImVec4 ACTIVE_COLOR = ImVec4(0, 0, 1, 1);
    const ImVec4 NO_TEXTURE_COLOR = ImVec4(0, 0, 0, 0);
    const ImVec4 DEFAULT_COLOR = ImVec4(1, 1, 1, 1);

    enum Tab {GROUNDS, BUILDINGS, UNITS, PLAYER, OVERALL};

    friend class PlayWindow;

    Tab tab = GROUNDS;
    
    static GameViewWindow* getInstance() {
      if (instance == nullptr) { instance = new GameViewWindow(); }
      return instance;
    }

    void draw() {
     

      ImGui::Begin(GAME_VIEW_WINDOW_NAME);

      ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
      if (ImGui::BeginTabBar("Section", tab_bar_flags))
      {
        if (ImGui::BeginTabItem("Grounds"))
        {
          tab = GROUNDS;
          ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Buildings"))
        {
          tab = BUILDINGS;
          ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Units"))
        {
          tab = UNITS;
          ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("PLAYER"))
        {
          tab = PLAYER;
          ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Overall"))
        {
          tab = OVERALL;
          ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
      }
      ImGui::Separator();


      if (tab == OVERALL) {
        draw_overall();
      }
      else if (tab == PLAYER) {
        draw_player();
      } 
      else {
        draw_ground();


        ImGui::Separator();

        ImGui::PushItemWidth(TEXT_INPUT_WIDTH);
        ImGui::InputText("X Max", buff_x_max, 4, ImGuiInputTextFlags_CharsDecimal);
        ImGui::SameLine();
        ImGui::InputText("Y Max", buff_y_max, 4, ImGuiInputTextFlags_CharsDecimal);
        ImGui::PopItemWidth();

        ImGui::SameLine();
        if (ImGui::Button("Change")) {
          int x = atoi(buff_x_max);
          int y = atoi(buff_y_max);
          reinit(x, y);
          setDefaultTexture(default_texture_id);
        }

        ImGui::PushItemWidth(TEXT_INPUT_WIDTH * 5);
        ImGui::InputText("File Name", buff_save_file, 20);
        ImGui::PopItemWidth();

        if (ImGui::Button("Save")) {

        }
        ImGui::SameLine();
        if (ImGui::Button("Load")) {

        }
      }

      ImGui::End();
    }

    char buff_x_max[4];
    char buff_y_max[4];
    char buff_save_file[20];
    

    void init(int x=10, int y=10) {
      grid_data = new GridData[x * y];
      x_max = x;
      y_max = y;

      strcpy_s(buff_x_max, std::to_string(x).c_str());
      strcpy_s(buff_y_max, std::to_string(y).c_str());

      for (int i = 0; i < x_max * y_max; i++) {
        grid_data[i].is_select = false;
        grid_data[i].tid = NULL;
        grid_data[i].building_tid = NULL;
        grid_data[i].unit_tid = NULL;
      }
    }

    void reinit(int x, int y) {
      deallow();
      init(x, y);
    }

    void setTexture(cw::Texture* texture) {
      if (tab == PLAYER) {
        player_texture = texture;
        return;
      }


      unsigned int tid = texture->tID;
      for (int i = 0; i < x_max * y_max; i++) {
        if (grid_data[i].is_select) { 
          switch (tab) {
          case GROUNDS:
            grid_data[i].tid = tid;
            break;
          case BUILDINGS:
            grid_data[i].building_tid = tid;
            break;
          case UNITS:
            grid_data[i].unit_tid = tid;
            break;
          }

          grid_data[i].is_select = false;
        }
      }
    }

    void setDefaultTexture(unsigned int tid) {
      default_texture_id = tid;
      for (int i = 0; i < x_max * y_max; i++) {
        if (grid_data[i].tid == NULL) { grid_data[i].tid = tid; }
      }
    }

    void setPlayerTexture(cw::Texture* t_player_texture) {
      player_texture = t_player_texture;
    }


  private:

    unsigned int get_tid(int i, int j) {
      int index = i * x_max + j;

      switch (tab) {
      case GROUNDS:
        return grid_data[index].tid;
        break;
      case BUILDINGS:
        return grid_data[index].building_tid;
        break;
      case UNITS:
        return grid_data[index].unit_tid;
        break;
      }

    }


    void draw_button(int i, int j){
      
      int index = i * x_max + j;

      ImGui::PushID((void*)(index));

      ImGui::PushStyleColor(ImGuiCol_Button, 
        grid_data[index].is_select ? ACTIVE_COLOR : DEACTIVE_COLOR
      );
      
      unsigned int tid = get_tid(i, j);
      
      ImVec4 b_color = tid == NULL ? NO_TEXTURE_COLOR : DEFAULT_COLOR;

      if (
        ImGui::ImageButton((void *)tid, ImVec2(32, 32), ImVec2(0,0), ImVec2(1, 1), 1, ImVec4(0,0,0,0), b_color)
        ) {
        grid_data[index].is_select = !grid_data[index].is_select;
      }
      ImGui::PopStyleColor();
      ImGui::PopID();
    }

    GameViewWindow() { init(); }
    virtual ~GameViewWindow() {
      deallow();
    }

    void deallow() {
      delete[] grid_data;
    }

    void draw_ground() {
      for (int i = 0; i < y_max; i++) {
        draw_button(i, 0);
        for (int j = 1; j < x_max; j++) {
          ImGui::SameLine();
          draw_button(i, j);
        }
      }
    }

    void draw_player() {
      if (player_texture == nullptr) { return; }
      ImVec2 offset = ImGui::GetItemRectMin();
      ImVec2 a = ImVec2(offset.x + 32, offset.y + 32);
      ImVec2 b = ImVec2(a.x + 32, a.y + 32);

      ImGui::GetWindowDrawList()->AddImage((void*) player_texture->tID, a, b);
    }

    void draw_overall(int i, int j) {
      int index = i * x_max + j;

      ImVec2 offset = ImGui::GetItemRectMin();
      ImVec2 a = ImVec2(offset.x + 32*j, offset.y + 32 * i);
      ImVec2 b = ImVec2(a.x + 32, a.y + 32);

      if (grid_data[index].tid != NULL) {
        ImGui::GetWindowDrawList()->AddImage((void*)grid_data[index].tid, a, b);
      }
      if (grid_data[index].building_tid != NULL) {
        ImGui::GetWindowDrawList()->AddImage((void*)grid_data[index].building_tid, a, b);
      }
      if (grid_data[index].unit_tid != NULL) {
        ImGui::GetWindowDrawList()->AddImage((void*)grid_data[index].unit_tid, a, b);
      }
    }

    void draw_overall() {

      for (int i = 0; i < y_max; i++) {
        draw_overall(i, 0);
        for (int j = 1; j < x_max; j++) {
          ImGui::SameLine();
          draw_overall(i, j);
        }
      }
   }

    static GameViewWindow* instance;

    GridData* grid_data;
    cw::Texture* player_texture;
    int x_max;
    int y_max;
    unsigned int default_texture_id = NULL;
   
  };

  GameViewWindow* GameViewWindow::instance = nullptr;
}
#endif
