/*
 * =====================================================================================
 *
 *         A simple game engine for ITGT533
 *
 *         Author:  Chatchai Wangwiwattana
 *
 * =====================================================================================
 */


#include "myApp.hpp"
#include "engine/gl_header.hpp"


#include <iostream>
#include <vector>
#include <string>


#include "engine/cw_app.hpp"
#include "engine/gl_shader.hpp"
#include "engine/gl_macros.hpp"
#include "engine/gl_mesh.hpp"
#include "engine/gl_texture.hpp"

#include "engine/cw_camera.hpp"
#include "engine/gl_data.hpp"

#include "engine/cw_sound.hpp"
#include "tools_window.hpp"
#include "game_view_window.hpp"
#include "play_window.hpp"

using glm::vec2;
using glm::vec3;
using glm::vec4;

namespace cw
{


    using namespace std;

    /*-----------------------------------------------------------------------------
     *  SHADER CODE
     *-----------------------------------------------------------------------------*/
    const char * vert = GLSL(120,

                             attribute vec3 position;
                             attribute vec3 normal;
                             attribute vec4 color;
                             attribute vec2 textureCoordinate;

                             uniform mat4 projection;
                             uniform mat4 view;
                             uniform mat4 model;
                             uniform mat3 normalMatrix;

                             varying vec2 texCoord;
                             varying float diffuse;
                             varying vec4 thecolor;

                             //the simplest function to calculate lighting
                             float doColor(){
                                 vec3 norm  = normalize( normalMatrix * normalize(normal) );
                                 vec3 light = normalize( vec3(1.0, 1.0, 1.0) );
                                 diffuse = max(dot(norm, light), 0.0 );

                                 return diffuse;
                             }

                             void main(void){
                                 thecolor = color;
                                 diffuse = doColor();
                                 texCoord = textureCoordinate;
                                 gl_Position = projection * view * model * vec4(position, 1);
                             }

                             );



    const char * frag = GLSL(120,

                             uniform sampler2D sampler;

                             varying vec2 texCoord;
                             varying float diffuse;
                             varying vec4 thecolor;

                             void main(void){
                                 gl_FragColor =  vec4( texture2D( sampler, texCoord ).rgb * thecolor.rgb * diffuse, 1.0);
                             }

                             );



    void MyApp::init()
    {
        // create shader program
        shader = new Shader( vert, frag );

        // Get uniform locations
        modelID = glGetUniformLocation(shader->id(), "model");
        viewID = glGetUniformLocation(shader->id(), "view");
        projectionID = glGetUniformLocation(shader-> id(), "projection");
        normalMatID = glGetUniformLocation(shader->id(), "normalMatrix");

        // Setup Camera
        m_camera = new Camera();
        m_camera->setPosition(glm::vec3(0, 0, 5));

        // Init input
        isMouseDown = false;
    }


    void MyApp::onUpdate( float dtSecond ){ }


    void MyApp::onDraw()
    {
        /*-----------------------------------------------------------------------------
         *  Draw GUI
         *-----------------------------------------------------------------------------*/

        dk::ToolsWindow::getInstance()->draw();
        dk::GameViewWindow::getInstance()->draw();
        dk::PlayWindow::getInstance()->draw();


        /*-----------------------------------------------------------------------------
         *  Draw Game World
         *-----------------------------------------------------------------------------*/

        // enable shader program
        shader->bind();

        // prepare matrix transformation
        glm::mat4 view = m_camera->getViewMat();
        glm::mat4 proj = m_camera->getProjectionMat(window().width(), window().height());
        glUniformMatrix4fv( viewID, 1, GL_FALSE, glm::value_ptr(view) );
        glUniformMatrix4fv( projectionID, 1, GL_FALSE, glm::value_ptr(proj) );

        //unsued shader program
        shader->unbind();

    }


    void MyApp::onKeyEvent(int key, int action)
    {
        /*-----------------------------------------------------------------------------
         *  exit the game when press esc
         *-----------------------------------------------------------------------------*/
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        {
            exit(0);
        }

        if (action == GLFW_RELEASE)
        {
          dk::PlayWindow::getInstance()->getKey(key);
        }

    }

    void MyApp::onMouseEvent(int button, int action)
    {
        /*-----------------------------------------------------------------------------
         *  Update mouse state to move camera
         *-----------------------------------------------------------------------------*/
        if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS )
        {
            isMouseDown = true;
        }
        if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE )
        {
            isMouseDown = false;
        }
    }

    void MyApp::onMouseMoveEvent(int x, int y)
    {

        /*-----------------------------------------------------------------------------
         *  Update mouse state to move camera
         *-----------------------------------------------------------------------------*/
        App::onMouseMoveEvent(x, y);

        //if( isMouseDown ) {        }

        previousX = x;
        previousY = y;
    }


    MyApp::~MyApp()
    {
        // clean up everything
        delete(shader);
        delete(m_camera);
    }

}
