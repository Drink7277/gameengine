#pragma once

#ifndef dk_play_window_hpp
#define dk_play_window_hpp

#include "window.hpp"
#include "game_view_window.hpp"
#include <map>

namespace fs = std::experimental::filesystem;

namespace dk {
  class PlayWindow : public Window {
  public:

    const char* PLAY_WINDOW_NAME = "Play";
    const char* SOUNDS_DIRECTORY = "sounds";
    const float OPTIONS_WIDTH = 200.0f;
    const float TEXT_INPUT_WIDTH = 30.0f;

    friend class GameViewWindow;

    static PlayWindow* getInstance() {
      if (instance == nullptr) { instance = new PlayWindow(); }
      return instance;
    }

    void draw() {
      ImGui::Begin(PLAY_WINDOW_NAME);

      ImGui::BeginChild("Options", ImVec2(OPTIONS_WIDTH, ImGui::GetWindowHeight()));
      draw_input_menu();
      draw_sound_menu();
      ImGui::EndChild();


      ImGui::SameLine();
      ImGui::BeginChild("overall");
      GameViewWindow::getInstance()->draw_overall();
      draw_player();
      ImGui::EndChild();
      ImGui::End();
    }

    void init() {
      player_pos = ImVec2(0, 0);

      fs::path current(std::experimental::filesystem::current_path());
      fs::path sounds_path(std::experimental::filesystem::current_path() / fs::path(SOUNDS_DIRECTORY));

      for (auto& entry : fs::directory_iterator(sounds_path)) {

        cw::Sound* sound = new cw::Sound(entry.path().u8string().c_str());

        file_sound.insert(
          std::make_pair(entry.path().filename().string(), sound)
        );
      }

      listbox_sounds = new char*[file_sound.size()];

      int i = 0;
      for (auto it = file_sound.begin(); it != file_sound.end(); it++, i++) {
        listbox_sounds[i] = (char *)(it->first.c_str());
      }

    }


    void getKey(int key) {

      if (key == up_key[0]) { move_up(); }
      if (key == down_key[0]) { move_down(); }
      if (key == left_key[0]) { move_left(); }
      if (key == right_key[0]) { move_right(); }

    }

    void move_up() {
      if (player_pos.y == 0) { return; }
      player_pos.y -= 1;
    }

    void move_down() {
      if (player_pos.y == GameViewWindow::getInstance()->y_max - 1) { return; }
      player_pos.y += 1;
    }

    void move_left() {
      if (player_pos.x == 0) { return; }
      player_pos.x -= 1;
    }

    void move_right() {
      if (player_pos.x == GameViewWindow::getInstance()->x_max - 1) { return; }
      player_pos.x += 1;
    }

  private:

    PlayWindow() { init(); }
    ~PlayWindow() {
      delete[] listbox_sounds;
      for (auto it = file_sound.begin(); it != file_sound.end(); it++) {
        delete it->second;
      }
    }

    void draw_sound_menu() {

      if (sound_loop) {
        play_sound();
      }

      if (ImGui::CollapsingHeader("Sound", ImGuiTreeNodeFlags_DefaultOpen)) {

        ImGui::PushItemWidth(OPTIONS_WIDTH);
        ImGui::ListBox("", &current_select_sound, listbox_sounds, file_sound.size(), 4);
        ImGui::PopItemWidth();

        if (ImGui::Button("Play")) {
          play_sound();
        }

        ImGui::SameLine();
        ImGui::Checkbox("Loop", &sound_loop);
      }
    }

    void play_sound(){
      if (last_select_sound != current_select_sound) {
        char* file = listbox_sounds[last_select_sound];
        file_sound.at(file)->stop();
      }

      last_select_sound = current_select_sound;
      char* file = listbox_sounds[current_select_sound];
      file_sound.at(file)->play();
    }

    void draw_input_menu() {
      if (ImGui::CollapsingHeader("Input", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::PushItemWidth(TEXT_INPUT_WIDTH);
        ImGui::InputText("Up key", up_key, 2, ImGuiInputTextFlags_CharsUppercase);

        ImGui::Separator();

        ImGui::InputText("Down key", down_key, 2, ImGuiInputTextFlags_CharsUppercase);

        ImGui::Separator();

        ImGui::InputText("Left key", left_key, 2, ImGuiInputTextFlags_CharsUppercase);

        ImGui::Separator();

        ImGui::InputText("Right key", right_key, 2, ImGuiInputTextFlags_CharsUppercase);
        ImGui::PopItemWidth();
      }
    }

    void draw_player() {
      cw::Texture* player_texture = GameViewWindow::getInstance()->player_texture;
      if (player_texture == nullptr) { return; }
      ImVec2 offset = ImGui::GetItemRectMin();
      ImVec2 a = ImVec2(offset.x + player_pos.x * 32, offset.y + player_pos.y * 32);
      ImVec2 b = ImVec2(a.x + 32, a.y + 32);
      ImGui::GetWindowDrawList()->AddImage((void*)player_texture->tID, a, b);
    }

    ImVec2 player_pos;

    char up_key[2] = "W";
    char left_key[2] = "A";
    char down_key[2] = "S";
    char right_key[2] = "D";
    bool sound_loop = true;
    int current_select_sound = 0;
    int last_select_sound = 0;

    char** listbox_sounds;
    std::map< std::string, cw::Sound * > file_sound;


    static PlayWindow* instance;
  };

  PlayWindow* PlayWindow::instance = nullptr;
}

#endif
