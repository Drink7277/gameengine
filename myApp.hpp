/*
 * =====================================================================================
 *
 *         A simple game engine for ITGT533
 *
 *         Author:  Chatchai Wangwiwattana
 *
 * =====================================================================================
 */

#ifndef myApp_h
#define myApp_h

#include "engine/cw_header.hpp"
#include "engine/gl_header.hpp"

#include "engine/cw_app.hpp"

#include "engine/cw_camera.hpp"

namespace cw
{
    class Shader;
    struct Mesh;
	struct Texture;

    class Sound;
    /*-----------------------------------------------------------------------------
     *  OUR APP
     *-----------------------------------------------------------------------------*/
    class MyApp final : public App{

    private:

        Shader *shader;
        Mesh *mesh;
        Mesh *mesh2;

        Texture *texture;
        Texture *texture2;

        cw::Camera *m_camera;

        //ID of Vertex Attribute
        GLuint positionID, normalID, colorID, textureCoorID;

        //A buffer ID
        GLuint bufferID, elementID;

        //An array ID
        GLuint arrayID;

        //ID of Uniforms
        GLuint modelID, viewID, projectionID, normalMatID;


        // keep input status
        bool isMouseDown;
        int previousX;
        int previousY;

        Sound* soundEffect1;

    public:
        MyApp() { init(); }
        ~MyApp();
        MyApp( const MyApp& ) = default;

        /*-----------------------------------------------------------------------------
         *  Overrided Functions
         *-----------------------------------------------------------------------------*/
        virtual void init();
        virtual void onUpdate( float dtSecond ) override;
        virtual void onDraw() override;
        virtual void onMouseMoveEvent(int x, int y) override;
        virtual void onMouseEvent(int button, int action) override;
        virtual void onKeyEvent(int key, int action) override;


    };

}
#endif /* myApp_h */
