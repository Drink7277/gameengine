#pragma once

#ifndef dk_window_hpp
#define dk_window_hpp

namespace dk {
  class Window {

  public:
    virtual void init() {}
    virtual void draw() {}
    virtual ~Window() { }

  };
}
#endif
